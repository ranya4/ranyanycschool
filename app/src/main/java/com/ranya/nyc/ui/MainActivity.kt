package com.ranya.nyc.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.lwlw.common.ViewUtil
import com.ranya.nyc.R
import com.ranya.nyc.adapter.MainActivityAdapterNycHighSchoolAdapter
import com.ranya.nyc.modelClasses.NycHighSchoolsModel
import com.ranya.nyc.viewModel.MainActivityViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private var mAdapter: MainActivityAdapterNycHighSchoolAdapter? = null
    private val nycHighSchoolsModel: MutableList<NycHighSchoolsModel> = mutableListOf()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mAdapter = MainActivityAdapterNycHighSchoolAdapter(this, nycHighSchoolsModel)
        val mLayoutManager = LinearLayoutManager(applicationContext)
        recyclerViewMainActivityNYCHighSchool.layoutManager = mLayoutManager
        recyclerViewMainActivityNYCHighSchool.itemAnimator = DefaultItemAnimator()
        recyclerViewMainActivityNYCHighSchool.adapter = mAdapter

        intiViewModel()
    }

    private fun intiViewModel() {
        progressBarMainActivity.visibility = View.VISIBLE
        val viewModel = ViewModelProvider(this).get(MainActivityViewModel::class.java)
        viewModel.getLiveData().observe(this, Observer {
            progressBarMainActivity.visibility = View.GONE
            if (it!=null){
                nycHighSchoolsModel.addAll(it)
                mAdapter?.notifyDataSetChanged()
                Log.e("TAG", "intiViewModel: ${nycHighSchoolsModel.size}", )
            }else{
                ViewUtil.showToast(this,"Empty List")
            }
        })
        viewModel.makeAPICall()
    }
}