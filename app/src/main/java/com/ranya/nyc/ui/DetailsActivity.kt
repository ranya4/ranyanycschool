package com.ranya.nyc.ui

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.lwlw.common.ViewUtil
import com.ranya.nyc.R
import com.ranya.nyc.modelClasses.GetSatScoreItem
import com.ranya.nyc.modelClasses.ModelClass
import com.ranya.nyc.retrofitServices.RetrofitClient
import kotlinx.android.synthetic.main.activity_details.emailIdDetails
import kotlinx.android.synthetic.main.activity_details.locationDetails
import kotlinx.android.synthetic.main.activity_details.overViewDetails
import kotlinx.android.synthetic.main.activity_details.phoneNoDetails
import kotlinx.android.synthetic.main.activity_details.satMathScoreDetails
import kotlinx.android.synthetic.main.activity_details.satReadingScoreDetails
import kotlinx.android.synthetic.main.activity_details.satWritingScoreDetails
import kotlinx.android.synthetic.main.activity_details.schoolNameDetails
import kotlinx.android.synthetic.main.activity_details.scoresLayout
import kotlinx.android.synthetic.main.activity_details.websiteDetails
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException


class DetailsActivity : AppCompatActivity() {
    private lateinit var location: String
    private lateinit var phoneNumber: String
    private lateinit var website: String
    private lateinit var schoolEmail: String
    private lateinit var overviewParagraph: String
    private lateinit var schoolName: String
    private lateinit var dbn: String
    private val getSatScoreItem: MutableList<GetSatScoreItem> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        val extras = intent.extras
        if (extras != null) {
            dbn = extras.getString("dbn").toString()
            location = extras.getString("location").toString()
            phoneNumber = extras.getString("phoneNumber").toString()
            website = extras.getString("website").toString()
            schoolEmail = extras.getString("schoolEmail").toString()
            overviewParagraph = extras.getString("overviewParagraph").toString()
            schoolName = extras.getString("schoolName").toString()
        }
        locationDetails.text = location
        phoneNoDetails.text = phoneNumber
        websiteDetails.text = website
        emailIdDetails.text = schoolEmail
        overViewDetails.text = overviewParagraph
        schoolNameDetails.text = schoolName

        callNumberAPI()
    }

    private fun callNumberAPI() {

        val service = RetrofitClient().webService
        CoroutineScope(Dispatchers.IO).launch {
            try {
                val response = service.getSatScores()
                withContext(Dispatchers.Main) {

                    when {
                        response!!.isSuccessful -> {
                            getSatScoresResponse(response.body())
                        }

                        else -> {
                            try {
                                assert(response.errorBody() != null)
                                val jsonResult = JSONObject(response.errorBody()!!.string())
                                val description = jsonResult["message"] as String
                                ViewUtil.showToast(applicationContext, description)
//                                Toast.makeText(this@EditStudentProfile, description, Toast.LENGTH_SHORT).show()
                            } catch (e: IOException) {
                                e.printStackTrace()
                            } catch (e: JSONException) {
                                e.printStackTrace()
                            }
                        }
                    }
                }
            } catch (ex: Exception) {
                withContext(Dispatchers.Main) {
                    Log.e("TAG", "callNumberAPI:   ${ex.localizedMessage}", )
                    ViewUtil.showToast(applicationContext, " ${ex.localizedMessage}")
                }
                ex.printStackTrace()
            }
        }
    }

    private fun getSatScoresResponse(body: List<GetSatScoreItem>?) {
        if (body != null) {
            scoresLayout.visibility = View.VISIBLE
            getSatScoreItem.addAll(body)
            for (position in 0..getSatScoreItem.size){
                if (getSatScoreItem[position].dbn == dbn){
                    satReadingScoreDetails.text = getSatScoreItem[position].satCriticalReadingAvgScore
                    satMathScoreDetails.text = getSatScoreItem[position].satMathAvgScore
                    satWritingScoreDetails.text = getSatScoreItem[position].satWritingAvgScore
                }
            }
        }
    }
}