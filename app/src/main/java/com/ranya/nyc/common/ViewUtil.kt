package com.lwlw.common

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.provider.Settings
import android.view.View
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import java.util.regex.Pattern

class ViewUtil {

    companion object {
        private var dialog: Dialog? = null

        const val PREFERENCE_NAME = "NYCHighSchools"

        fun showLoader(context: Context, outsideTouch: Boolean, cancel: Boolean) {
            if (dialog == null) {
                dialog = Dialog(context)
                dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
                //dialog!!.setContentView(R.layout.item_dialog_loading_view)
            }

//            dialog!!.getWindow()
//                ?.setBackgroundDrawable(context.resources.getDrawable(R.drawable.d_round_white_background))
            if (!outsideTouch) dialog!!.setCanceledOnTouchOutside(
                    false)
            if (!cancel) dialog!!.setCancelable(false)
            dialog!!.show()
        }

        fun cancelLoader() {
            dialog?.cancel()
        }


        fun showToast(context: Context, msg: String) {
            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
        }


        fun savePreferences(context: Context, key: String, value: Any) {
            val sharedPreference =
                context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE)
            val editor = sharedPreference?.edit()
            when (value) {
                is Int -> {
                    editor?.putInt(key, value)?.apply()
                }
                is String -> {
                    editor?.putString(key, value)?.apply()
                }
                is Boolean -> {
                    editor!!.putBoolean(key, value).apply()
                }
            }
        }


        fun isNetworkAvailable(context: Context): Boolean {
            val connectivityManager =
                context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val nw = connectivityManager.activeNetwork ?: return false
                val actNw = connectivityManager.getNetworkCapabilities(nw) ?: return false
                return when {
                    actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                    actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                    //for other device how are able to connect with Ethernet
                    actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                    //for check internet over Bluetooth
                    actNw.hasTransport(NetworkCapabilities.TRANSPORT_BLUETOOTH) -> true
                    else -> false
                }
            } else {
                val nwInfo = connectivityManager.activeNetworkInfo ?: return false
                return nwInfo.isConnected
            }
        }

        fun internetSettings(ctx: Context) {
            val alertDialogBuilder = AlertDialog.Builder(ctx)
            alertDialogBuilder.setMessage("No internet connection on your device. Would you like to enable it?")
                .setTitle("No Internet Connection").setCancelable(false).setPositiveButton(
                    " Enable Internet "
                ) { dialog: DialogInterface?, id: Int ->
                    val dialogIntent = Intent(Settings.ACTION_SETTINGS)
                    dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    ctx.startActivity(dialogIntent)
                }
            alertDialogBuilder.setNegativeButton(
                " Cancel "
            ) { dialog: DialogInterface, id: Int -> dialog.cancel() }
            val alert = alertDialogBuilder.create()
            alert.show()
        }
    }
}