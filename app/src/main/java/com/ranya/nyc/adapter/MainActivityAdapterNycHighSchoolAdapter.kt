package com.ranya.nyc.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ranya.nyc.R
import com.ranya.nyc.modelClasses.NycHighSchoolsModel
import com.ranya.nyc.ui.DetailsActivity
import com.ranya.nyc.ui.MainActivity
import kotlinx.android.synthetic.main.nyc_high_school_list.view.*

class MainActivityAdapterNycHighSchoolAdapter(
    val mainActivity: MainActivity,
    var nycHighSchoolsModel: MutableList<NycHighSchoolsModel>
) : RecyclerView.Adapter<MainActivityAdapterNycHighSchoolAdapter.MyViewHolder>() {


    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.adapterLayoutNYCHighSchool.setOnClickListener {
                val intent = Intent(mainActivity, DetailsActivity::class.java)
                intent.putExtra("dbn", nycHighSchoolsModel[adapterPosition].dbn.toString())
                intent.putExtra("location", nycHighSchoolsModel[adapterPosition].location.toString())
                intent.putExtra("phoneNumber", nycHighSchoolsModel[adapterPosition].phoneNumber.toString())
                intent.putExtra("website", nycHighSchoolsModel[adapterPosition].website.toString())
                intent.putExtra("schoolEmail", nycHighSchoolsModel[adapterPosition].schoolEmail.toString())
                intent.putExtra("overviewParagraph", nycHighSchoolsModel[adapterPosition].overviewParagraph.toString())
                intent.putExtra("schoolName", nycHighSchoolsModel[adapterPosition].schoolName.toString())
                mainActivity.startActivity(intent)
            }
        }
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView: View =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.nyc_high_school_list, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.itemView.schoolNameMainAdapter.text = nycHighSchoolsModel[position].schoolName.toString()
    }

    override fun getItemCount(): Int {
        return nycHighSchoolsModel.size
    }
}