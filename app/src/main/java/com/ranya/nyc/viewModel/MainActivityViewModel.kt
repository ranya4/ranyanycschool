package com.ranya.nyc.viewModel

import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.lwlw.common.ViewUtil
import com.ranya.nyc.modelClasses.NycHighSchoolsModel
import com.ranya.nyc.retrofitServices.RetrofitClient
import com.ranya.nyc.ui.MainActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException

class MainActivityViewModel:ViewModel() {
    var liveDataList: MutableLiveData<List<NycHighSchoolsModel>> = MutableLiveData()

    fun getLiveData(): MutableLiveData<List<NycHighSchoolsModel>>{
        return liveDataList
    }

    fun makeAPICall(){
        Log.e("TAG", "makeAPICall: 1", )
        val service = RetrofitClient().webService
        CoroutineScope(Dispatchers.IO).launch {
            try {
                val response = service.getNycHighSchoolsData()
                withContext(Dispatchers.Main) {
                    when {
                        response!!.isSuccessful -> {
                            Log.e("TAG", "makeAPICall: 3", )
                            liveDataList.postValue(response.body())
                            Log.e("TAG", "makeAPICall: 3${response.body()}", )
                            //loginRequestResponse(response.body())
                        }
                        else -> {
                            Log.e("TAG", "makeAPICall: 3", )
                            liveDataList.postValue(null)
                        }
                    }
                }
            } catch (ex: Exception) {
                withContext(Dispatchers.Main) {
                    Log.e("TAG", "makeAPICall:   ${ex.localizedMessage}", )
                    ex.printStackTrace()
                    liveDataList.postValue(null)
                }
            }
        }
    }

}