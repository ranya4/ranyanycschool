package com.ranya.nyc.retrofitServices

import com.ranya.nyc.modelClasses.GetSatScoreItem
import com.ranya.nyc.modelClasses.ModelClass
import com.ranya.nyc.modelClasses.NycHighSchoolsModel
import retrofit2.*
import retrofit2.http.*

interface WebService {

    @GET("resource/s3k6-pzi2")
    suspend fun getNycHighSchoolsData(): Response<List<NycHighSchoolsModel>>?

    @GET("resource/f9bf-2cp4")
    suspend fun getSatScores(): Response<ArrayList<GetSatScoreItem>>?

}
